﻿namespace Otus.Teaching.Pcf.ReceivingFromPartner.Core.Abstractions.Data
{
    public interface IDbInitializer
    {
        void InitializeDb();
    }
}