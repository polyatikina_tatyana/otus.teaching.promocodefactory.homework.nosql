﻿namespace Otus.Teaching.Pcf.Administration.Core.Abstractions.Data
{
    public interface IDbInitializer
    {
        void InitializeDb();
    }
}