using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Configuration;
using Otus.Teaching.Pcf.Administration.DataAccess.Repositories;
using IConfiguration = Microsoft.Extensions.Configuration.IConfiguration;
using Otus.Teaching.Pcf.Administration.Core.Abstractions.Repositories;
using Otus.Teaching.Pcf.Administration.Core.Abstractions.Data;
using Otus.Teaching.Pcf.Administration.Mongo.Options;
using Otus.Teaching.Pcf.Administration.MongoDataAccess.Data;
using Otus.Teaching.Pcf.Administration.MongoDataAccess;
using System;
using Microsoft.Extensions.Logging;

namespace Otus.Teaching.Pcf.Administration.WebHost
{
    public class Startup
    {
        public IConfiguration Configuration { get; }

        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }
        
        // This method gets called by the runtime. Use this method to add services to the container.
        // For more information on how to configure your application, visit https://go.microsoft.com/fwlink/?LinkID=398940
        public void ConfigureServices(IServiceCollection services)
        {
            services
                .AddControllers()
                .AddMvcOptions(x => x.SuppressAsyncSuffixInActionNames = false);

            services.AddScoped(typeof(IRepository<>), typeof(MongoRepository<>));
            services.AddScoped<IDbInitializer, MongoDbInitializer>();

            services.ConfigureMongoService();

            //services.AddDbContext<DataContext>(x =>
            //{
            //    x.UseNpgsql(Configuration.GetConnectionString("PromocodeFactoryAdministrationDb"));
            //    x.UseSnakeCaseNamingConvention();
            //    x.UseLazyLoadingProxies();
            //});

            services.AddSingleton(serviceProvider => new MongoDataContext(new MongoConnectionSetting
            {
                ConnectionString = Configuration.GetConnectionString("PromocodeFactoryAdministrationMongoDb"),
                DatabaseName = "PromocodeFactoryAdministrationDb"
            }));

            
            services.AddOpenApiDocument(options =>
            {
                options.Title = "PromoCode Factory Administration API Doc";
                options.Version = "1.0";
            });
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app,
            IWebHostEnvironment env,
            IHostApplicationLifetime lifetime)
        {
            lifetime.ApplicationStarted.Register(() => OnApplicationStarted(app.ApplicationServices, lifetime));

            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseHsts();
            }

            app.UseOpenApi();
            app.UseSwaggerUi3(x =>
            {
                x.DocExpansion = "list";
            });
            
            app.UseHttpsRedirection();

            app.UseRouting();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }

        public static void OnApplicationStarted(IServiceProvider serviceProvider, IHostApplicationLifetime lifetime)
        {
            try
            {
                using var scope = serviceProvider.CreateScope();
                var logger = scope.ServiceProvider.GetRequiredService<ILogger<Startup>>();                
                try
                {
                    logger.LogWarning("Start initialize db");

                    var dbInitializer = scope.ServiceProvider.GetRequiredService<IDbInitializer>();
                    dbInitializer.InitializeDb();

                    logger.LogWarning("Stop initialize db");
                }
                catch (Exception ex)
                {
                    logger.LogError(ex, nameof(OnApplicationStarted));
                    throw;
                }
            }
            catch
            {
                lifetime.StopApplication();
            }
        }
    }
}