﻿namespace Otus.Teaching.Pcf.Administration.Mongo.Options
{
    public class MongoConnectionSetting
    {
        public string ConnectionString { get; set; }
        public string DatabaseName { get; set; }
    }
}
