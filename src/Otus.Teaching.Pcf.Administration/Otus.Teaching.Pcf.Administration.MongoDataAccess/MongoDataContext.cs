﻿using MongoDB.Driver;
using Otus.Teaching.Pcf.Administration.Core.Domain.Administration;
using Otus.Teaching.Pcf.Administration.Mongo;
using Otus.Teaching.Pcf.Administration.Mongo.Options;

namespace Otus.Teaching.Pcf.Administration.MongoDataAccess
{
    public class MongoDataContext : MongoDbContext
    {
        public MongoDataContext(MongoConnectionSetting connectionString)
            : base(connectionString)
        {
            RegisterCollection<Role>(nameof(Roles));
            RegisterCollection<Employee>(nameof(Employees), "roleId");
        }
                
        public IMongoCollection<Role> Roles => GetCollections<Role>();

        public IMongoCollection<Employee> Employees => GetCollections<Employee>();
    }
}
