﻿using MongoDB.Driver;
using Otus.Teaching.Pcf.Administration.MongoDataAccess;
using Otus.Teaching.Pcf.Administration.Core.Abstractions.Repositories;
using Otus.Teaching.Pcf.Administration.Core.Domain;
using Otus.Teaching.Pcf.Administration.Mongo.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace Otus.Teaching.Pcf.Administration.DataAccess.Repositories
{
    public class MongoRepository<T>
        : MongoDbRepository<T, MongoDataContext>, IRepository<T>
        where T : BaseEntity
    {
        public MongoRepository(MongoDataContext mongoDataContext)
            : base(mongoDataContext)
        {
        }
    }
}
