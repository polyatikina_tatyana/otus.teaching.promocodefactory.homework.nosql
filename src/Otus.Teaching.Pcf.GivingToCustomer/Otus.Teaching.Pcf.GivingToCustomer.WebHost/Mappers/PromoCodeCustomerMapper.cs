﻿using Otus.Teaching.Pcf.GivingToCustomer.Core.Domain;
using Otus.Teaching.Pcf.GivingToCustomer.Core.Dto;

namespace Otus.Teaching.Pcf.GivingToCustomer.WebHost.Mappers
{
    public static class PromoCodeCustomerMapper
    {
        public static PromoCodeCustomer MapFromModel(Customer customer, PromoCode promoCode)
        {
            return new PromoCodeCustomer
            {
                CustomerId = customer.Id,
                Customer = new CustomerDto
                {
                    Id = customer.Id,
                    FirstName = customer.FirstName,
                    LastName = customer.LastName
                },
                PromoCode = promoCode,
                PromoCodeId = promoCode.Id
            };
        }
    }
}
