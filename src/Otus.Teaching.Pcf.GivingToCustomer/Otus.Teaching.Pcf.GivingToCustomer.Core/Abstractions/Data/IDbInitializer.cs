﻿namespace Otus.Teaching.Pcf.GivingToCustomer.Core.Abstractions.Data
{
    public interface IDbInitializer
    {
        void InitializeDb();
    }
}