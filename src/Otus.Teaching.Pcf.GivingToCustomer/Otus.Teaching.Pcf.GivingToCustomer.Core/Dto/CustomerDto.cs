﻿using System;

namespace Otus.Teaching.Pcf.GivingToCustomer.Core.Dto
{
    public class CustomerDto
    {
        public Guid Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string FullName => $"{FirstName} {LastName}";
    }
}
