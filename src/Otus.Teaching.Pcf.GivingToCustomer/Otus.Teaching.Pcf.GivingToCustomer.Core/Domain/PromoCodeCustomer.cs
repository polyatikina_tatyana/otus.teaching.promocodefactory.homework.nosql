﻿using System;
using Otus.Teaching.Pcf.GivingToCustomer.Core.Dto;

namespace Otus.Teaching.Pcf.GivingToCustomer.Core.Domain
{
    public class PromoCodeCustomer
    {
        public Guid PromoCodeId { get; set; }
        public virtual PromoCode PromoCode { get; set; }

        public Guid CustomerId { get; set; }
        public virtual CustomerDto Customer { get; set; }
    }
}
