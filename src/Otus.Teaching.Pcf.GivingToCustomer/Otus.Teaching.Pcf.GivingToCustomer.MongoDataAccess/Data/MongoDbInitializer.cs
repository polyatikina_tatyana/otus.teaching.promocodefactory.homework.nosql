﻿using Otus.Teaching.Pcf.GivingToCustomer.Core.Abstractions.Data;
using Otus.Teaching.Pcf.GivingToCustomer.Core.Domain;

namespace Otus.Teaching.Pcf.GivingToCustomer.MongoDataAccess.Data
{
    public class MongoDbInitializer : IDbInitializer
    {
        private readonly MongoDataContext _mongoDataContext;

        public MongoDbInitializer(MongoDataContext mongoDataContext)
        {
            _mongoDataContext = mongoDataContext;
        }

        
        public void InitializeDb()
        {
            _mongoDataContext.DropCollection<PromoCode>();

            _mongoDataContext.DropCollection<Preference>();

            if (_mongoDataContext.Count(_mongoDataContext.Preferences) == 0)
            {
                _mongoDataContext.AddRangeAsync(_mongoDataContext.Preferences, FakeDataFactory.Preferences);
            }

            _mongoDataContext.DropCollection<Customer>();

            if (_mongoDataContext.Count(_mongoDataContext.Customers) == 0)
            {
                _mongoDataContext.AddRangeAsync(_mongoDataContext.Customers, FakeDataFactory.Customers);
            }
        }
    }
}