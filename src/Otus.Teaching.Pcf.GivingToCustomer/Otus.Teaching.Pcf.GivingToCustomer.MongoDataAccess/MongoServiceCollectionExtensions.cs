﻿using MongoDB.Bson.Serialization.Serializers;
using MongoDB.Bson.Serialization;
using MongoDB.Bson;
using Microsoft.Extensions.DependencyInjection;
using Otus.Teaching.Pcf.GivingToCustomer.Core.Domain;
using Otus.Teaching.Pcf.GivingToCustomer.Mongo;

namespace Otus.Teaching.Pcf.GivingToCustomer.MongoDataAccess
{
    public static class MongoServiceCollectionExtensions
    {
        public static IServiceCollection ConfigureMongoService(this IServiceCollection services)
        {
            MongoConventionsConfigurator.CreateDefault();

            BsonSerializer.RegisterSerializer(new GuidSerializer(BsonType.String));

            RegisterClassMaps();

            return services;
        }

        private static void RegisterClassMaps()
        {
            BsonClassMap.RegisterClassMap<PromoCode>(map =>
            {
                map.AutoMap();

                map.MapMember(emp => emp.PartnerId)
                   .SetSerializer(new GuidSerializer(BsonType.String));

                map.MapMember(emp => emp.PreferenceId)
                   .SetSerializer(new GuidSerializer(BsonType.String));

                map.MapMember(code => code.BeginDate).SetSerializer(new DateTimeSerializer(true));
                map.MapMember(code => code.EndDate).SetSerializer(new DateTimeSerializer(true));

                map.SetIgnoreExtraElements(true);
            });

            BsonClassMap.RegisterClassMap<Customer>(map =>
            {
                map.AutoMap();

                map.UnmapMember(p => p.FullName);

                map.SetIgnoreExtraElements(true);
            });

            BsonClassMap.RegisterClassMap<Preference>(map =>
            {
                map.AutoMap();

                map.SetIgnoreExtraElements(true);
            });

            BsonClassMap.RegisterClassMap<PromoCodeCustomer>(map =>
            {
                map.AutoMap();
                map.MapMember(p => p.CustomerId)
                   .SetSerializer(new GuidSerializer(BsonType.String));
                map.MapMember(p => p.PromoCodeId)
                   .SetSerializer(new GuidSerializer(BsonType.String));

                map.SetIgnoreExtraElements(true);
            });

            BsonClassMap.RegisterClassMap<CustomerPreference>(map =>
            {
                map.AutoMap();
                map.MapMember(p => p.CustomerId)
                   .SetSerializer(new GuidSerializer(BsonType.String));
                map.MapMember(p => p.PreferenceId)
                   .SetSerializer(new GuidSerializer(BsonType.String));

                map.SetIgnoreExtraElements(true);
            });
        }
    }
}
