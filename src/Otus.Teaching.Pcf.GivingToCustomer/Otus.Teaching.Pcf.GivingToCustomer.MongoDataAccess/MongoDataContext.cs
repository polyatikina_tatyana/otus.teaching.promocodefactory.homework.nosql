﻿using MongoDB.Driver;
using Otus.Teaching.Pcf.GivingToCustomer.Core.Domain;
using Otus.Teaching.Pcf.GivingToCustomer.Mongo.Options;
using Otus.Teaching.Pcf.GivingToCustomer.Mongo;

namespace Otus.Teaching.Pcf.GivingToCustomer.MongoDataAccess
{
    public class MongoDataContext : MongoDbContext
    {
        public MongoDataContext(MongoConnectionSetting connectionString)
            : base(connectionString)
        {
            RegisterCollection<Preference>(nameof(Preferences));
            RegisterCollection<PromoCode>(nameof(PromoCodes));
            RegisterCollection<Customer>(nameof(Customers));
        }

        public IMongoCollection<PromoCode> PromoCodes => GetCollections<PromoCode>();
        
        public IMongoCollection<Customer> Customers => GetCollections<Customer>();

        public IMongoCollection<Preference> Preferences => GetCollections<Preference>();
    }
}