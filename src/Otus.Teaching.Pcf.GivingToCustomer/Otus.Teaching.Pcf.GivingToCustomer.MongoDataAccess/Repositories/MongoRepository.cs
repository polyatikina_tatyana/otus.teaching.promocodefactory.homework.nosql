﻿using Otus.Teaching.Pcf.GivingToCustomer.Core.Abstractions.Repositories;
using Otus.Teaching.Pcf.GivingToCustomer.Core.Domain;
using Otus.Teaching.Pcf.GivingToCustomer.Mongo.Repositories;

namespace Otus.Teaching.Pcf.GivingToCustomer.MongoDataAccess.Repositories
{
    public class MongoRepository<T>
        : MongoDbRepository<T, MongoDataContext>, IRepository<T>
        where T : BaseEntity
    {
        public MongoRepository(MongoDataContext mongoDataContext)
            : base(mongoDataContext)
        {
        }
    }
}