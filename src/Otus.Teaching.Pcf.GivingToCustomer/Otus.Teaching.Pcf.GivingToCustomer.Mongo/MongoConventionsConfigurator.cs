﻿using MongoDB.Bson.Serialization.Conventions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Otus.Teaching.Pcf.GivingToCustomer.Mongo
{
    public static class MongoConventionsConfigurator
    {
        public static void CreateDefault()
        {
            var pack = new ConventionPack
            {
                new CamelCaseElementNameConvention(),
            };

            ConventionRegistry.Register("CamelCase", pack, t => true);
        }
    }
}
